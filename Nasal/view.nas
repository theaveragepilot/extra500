#    This file is part of extra500
#
#    extra500 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    extra500 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with extra500.  If not, see <http://www.gnu.org/licenses/>.
#
#      Authors: Dirk Dittmann
#      Date: 31.01.2015
#
#      Last change: Eric van den Berg	      
#      Date:  18.11.2018           
#
#
# 
#

var currentViewSettings = {
	fieldOfView 	: getprop("/sim/current-view/field-of-view"),
	goalHeading 	: getprop("/sim/current-view/goal-heading-offset-deg"),
	goalPitch	: getprop("/sim/current-view/goal-pitch-offset-deg")
	
};

var quickZoomActive = 0;

var quickZoomView = func(){
	#print("View.quickZoomView() ...");
	
	if (quickZoomActive == 1 ){
		# reset to the old
		
		setprop("/sim/current-view/field-of-view",currentViewSettings.fieldOfView);
		setprop("/sim/current-view/goal-heading-offset-deg",currentViewSettings.goalHeading);
		setprop("/sim/current-view/goal-pitch-offset-deg",currentViewSettings.goalPitch);
		
                quickZoomActive=0;
		
	}else{
		# set quick view
		
                currentViewSettings.fieldOfView = getprop("/sim/current-view/field-of-view");
                currentViewSettings.goalHeading = getprop("/sim/current-view/goal-heading-offset-deg");
                currentViewSettings.goalPitch = getprop("/sim/current-view/goal-pitch-offset-deg");
                currentViewSettings.viewNumber = getprop("/sim/current-view/view-number");
                
		setprop("/sim/current-view/field-of-view",getprop("/extra500/config/view/quickZoom/field-of-view"));
		setprop("/sim/current-view/goal-heading-offset-deg",getprop("/extra500/config/view/quickZoom/goal-heading-offset-deg"));
		setprop("/sim/current-view/goal-pitch-offset-deg",getprop("/extra500/config/view/quickZoom/goal-pitch-offset-deg"));
		
                quickZoomActive=1;
	}
	

};

var saveCurrentViewAsQuickZoomView = func(){
    setprop("/extra500/config/view/quickZoom/field-of-view",getprop("/sim[0]/current-view[0]/field-of-view"));
    setprop("/extra500/config/view/quickZoom/goal-heading-offset-deg",getprop("/sim[0]/current-view[0]/goal-heading-offset-deg"));
    setprop("/extra500/config/view/quickZoom/goal-pitch-offset-deg",getprop("/sim[0]/current-view[0]/goal-pitch-offset-deg"));

	UI.msg.info("Quick view defined");    
	UI.msg.info("Toggle to and from this view with <");                                                                    
};

setlistener("/sim/current-view/field-of-view", func(n) {
            quickZoomActive = 0;
}, 1);
# 
# setlistener("/sim/current-view/goal-heading-offset-deg", func(n) {
#             quickZoomActive = 0;
# }, 1);
# 
# setlistener("/sim/current-view/goal-pitch-offset-deg", func(n) {
#             quickZoomActive = 0;
# }, 1);

setlistener("/sim/current-view/view-number", func(n) {
	var v = view.current.getNode("config");
	var yOffset = v.getNode("y-offset-m", 1).getValue() or 0;
	#print("yOffset",yOffset);
        quickZoomActive =0;
	setprop("/sim/current-view/y-offset-m-config", yOffset);
}, 1);
